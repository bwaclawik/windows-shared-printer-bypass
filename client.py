#!/usr/bin/env python3
import socket, sys, os, json, time, itertools
from frame import WSPB_frame


# read config file
if len(sys.argv) != 2:
    sys.exit("You need to provide a config file as the only argument")
try:
    with open(sys.argv[1], 'r') as config_file:
        config = json.load(config_file)

    SERVER_ADDRESS = (config['server_address'], config['server_port'])
    PRINTER_NAME = config['printer']
    RECONNECT_DELAY = int(config['reconnect_delay'])
    CONNECT_ATTEMPTS = int(config['connect_attempts'])
except:
    sys.exit("Cannot correctly parse config file")


# read printing data from stdin
data = bytes()
while True:
    new_data = sys.stdin.buffer.read(64 * 1024)
    if not new_data:
        break
    data += new_data
    if len(data) > WSPB_frame.MAX_DATA_SIZE:
        sys.exit('Input data is too big - terminating')
print(f'Read {len(data)} bytes from stdin', file=sys.stderr)


# read metadata from environment variables
job_name = os.environ.get('REDMON_DOCNAME', '???')
machine_name = os.environ.get('REDMON_MACHINE', '???')


# send data to the server
for attempt in range(CONNECT_ATTEMPTS) if CONNECT_ATTEMPTS > 0 else itertools.count():
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect(SERVER_ADDRESS)
            frame = WSPB_frame(data, job_name, machine_name, PRINTER_NAME)
            sock.send(frame.encode())
        break

    except OSError as exc:
        print(f'Connection attempt #{attempt} has failed : {str(exc)}', file=sys.stderr)
        time.sleep(RECONNECT_DELAY)
else:
    sys.exit('Sending has failed too many times')
