# Windows Shared Printer Bypass 🖧🖶

Small program intended to provide alternative printer sharing infrastructure in Microsoft Windows systems.
It is meant to be used when standard mechanisms don't work, e.g. due to mysterious errors during connecting to a shared printer (like 0x00000040 etc.) many people encounter.


# How it works?

The program is based on unidirectional client-server architecture (but there can be multiple servers).
The server is the computer that is able to directly communicate with a printer (e.g. is connected to it with a cable).
The clients are the computers that want to print remotely.

On a client side, a virtual printer is added that represents an actual printer available on the server.
It is controlled by the original printer driver (just as it was a local printer), but has customly set output port.
When a print job is started (in a standard way), the data (already processed by a driver) is passed to WSPB client program, which in turn sends it to the server using its own protocol.
Server, after receiving the data, simply adds a new print job to its local printer queue and writes the received raw data to it.

Server program runs as a Windows service.
Client program, on the other hand, works only while printing, so that every time remote print job is created, a new WSPB client instance is started (and it stops right after sending the data).


# Dependencies

The program is written and tested in Python 3.8, although any modern Python version should work as well.  
It uses [pywin32](https://github.com/mhammond/pywin32) library.

Client program is designed to be used with [RedMon](http://www.ghostgum.com.au/software/redmon.htm) port monitor.
On the project's website there's an information that support for Windows 10 is not planned, but it turns out that it works anyway (at least to extent required by WSPB).


# Instalation

Instructions are given for Windows 10.

## Server side

1. Prepare configuration file and save it to directory "C:\\WSPB\\server_config.json".
2. Open Command Prompt (cmd.exe) using "Run as administrator" option.
3. Install server service using command `python server.py install`.
4. Set service's startup mode to "auto" using system's "Services" panel.

## Client side

Before adding a printer, make sure that it's properly installed on the server machine and it has a corresponding entry in server's WSPB config file.
Keep in mind that the printer doesn't have to be set as shared (but it can be if desired).

In order to add a remote printer:
1. If not installed yet, install RedMon on a client computer.
2. Prepare a configuration file for a new printer.
3. Install the printer on the client machine in a regular way, as if it was connected to that computer directly. Only a bare printer driver is needed (any fancy venor-specific features probably won't work anyway).
4. Open Command Prompt (cmd.exe) using "Run as administrator" option.
5. Show the printer settings window using `printui /p /n"PRINTER NAME HERE"` command.
6. Create new port of type "Redirected port", select it for this printer and edit its settings to:
    | Parameter | Value |
    | --- | --- |
    | "Redirect this port to a program" | WSPB client executable |
    | "Arguments for this program are" | config file path |
    | "Output" | "Program handles output" |
    | "Print errors" | unchecked |
    | "Run as user" | checked |

# Configuration files

Configuration files, for both client and server, are JSON files.
Server needs one config file, while client needs one config file per printer.
All parameters are key-value pairs of top-level object and all of them need to be present (there are no hardcoded default values).

Server configurations:
| Key | Type | Description |
| --- | --- | --- |
| `address` | String | IPv4 address of local network interface which server should listen on. Empty string means listening on any intertface and is a good default value.
| `port` | Number | TCP port number for incoming connections.
| `printers` | Object | Mapping of printers names. For each printer there should be a key-value pair where key is a remote name and value is a real name (as set in Windows printers panel).

After modifying server's configuration, the service needs to be restarted.

Client configurations:
| Key | Type | Description |
| --- | --- | --- |
| `server_address` | String | Address of the server. It can be either IPv4 address or computer name (when Windows workgroup is in use). |
| `server_port` | Number | TCP port number of the server. |
| `printer` | String | Remote printer name, matching an entry in server's `printers` parameter |
| `reconnect_delay` | Number | Delay before consecutive reconnection attempts, in seconds. |
| `connect_attempts` | Number | Number of connection attempts. If the value is nonpositive, it means infinite attempts. |

Example configuration files are provided with a code.


# Disclaimer

This software has been written as a workaround, not a production grade product, and will remain as such.
It is only meant to handle simple use cases.
In particular, it lacks important security features (such as encryption or permission control) and will probably fail in some setups (e.g. with some printer models or network configurations).
