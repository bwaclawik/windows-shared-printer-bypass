#!/usr/bin/env python3
import ctypes


# Winapi definitions
class WinSpool:
    _winspool = ctypes.windll.LoadLibrary('winspool.drv')
    OpenPrinter = _winspool.OpenPrinterW
    ClosePrinter = _winspool.ClosePrinter
    StartDocPrinter = _winspool.StartDocPrinterW
    EndDocPrinter = _winspool.EndDocPrinter
    StartPagePrinter = _winspool.StartPagePrinter
    EndPagePrinter = _winspool.EndPagePrinter
    WritePrinter = _winspool.WritePrinter

    class PRINTER_DEFAULTS(ctypes.Structure):
        _fields_ = [
            ('pDatatype', ctypes.c_char_p),
            ('pDevMode', ctypes.c_void_p),
            ('DesiredAccess', ctypes.c_uint)
        ]
        PRINTER_ACCESS_USE = 0x00000008

    class DOC_INFO_1(ctypes.Structure):
        _fields_ = [
            ('pDocName', ctypes.c_wchar_p),
            ('pOutputFile', ctypes.c_wchar_p),
            ('pDatatype', ctypes.c_wchar_p)
        ]


# Printer class
class Printer:
    def __init__(self, name):
        self.name = name
        self.handle = ctypes.c_void_p(None)
    
    def __enter__(self):
        if not WinSpool.OpenPrinter(self.name, ctypes.byref(self.handle), ctypes.byref(WinSpool.PRINTER_DEFAULTS(None, None, WinSpool.PRINTER_DEFAULTS.PRINTER_ACCESS_USE))):
            raise OSError(f'Cannot open printer "{self.name}"')
        return self
    
    def __exit__(self, nm1, nm2, nm3):
        WinSpool.ClosePrinter(self.handle)
    
    class PrintJob:
        def __init__(self, printer, name, source):
            self.printer = printer
            self.name = name
            self.source = source
        
        def __enter__(self):
            name = f'WSPB: {self.name} from {self.source}'
            if not WinSpool.StartDocPrinter(self.printer.handle, 1, ctypes.byref(WinSpool.DOC_INFO_1(name, None, 'RAW'))):
                raise OSError(f'Cannot start print job')
            return self
        
        def __exit__(self, nm1, nm2, nm3):
            WinSpool.EndDocPrinter(self.printer.handle)
        
        class Page:
            def __init__(self, printer):
                self.printer = printer
            
            def __enter__(self):
                if not WinSpool.StartPagePrinter(self.printer.handle):
                    raise OSError(f'Cannot start new page')
                return self

            def __exit__(self, nm1, nm2, nm3):
                WinSpool.EndPagePrinter(self.printer.handle)
            
            def write(self, data : bytes):
                while data:
                    written = ctypes.c_int32()
                    if not WinSpool.WritePrinter(self.printer.handle, data, len(data), ctypes.byref(written)):
                        raise OSError('Cannot write to the printer')
                    data = data[written.value:]

        def new_page(self):
            return self.Page(self.printer)
    
    def new_print_job(self, job_name, machine_name):
        return self.PrintJob(self, job_name, machine_name)