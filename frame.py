#!/usr/bin/env python3
import struct, zlib

# Frame format
# 4B - data size
# 4B - job name encoded size
# 4B - machine name encoded size
# 4B - printer name encoded size
# *  - data (bytes)
# *  - job name (utf-8)
# *  - machine name (utf-8)
# *  - printer name (utf-8)
# 4B - Adler32 checksum
#
# all numbers are unsigned, big endian

class WSPB_frame:
    MAX_DATA_SIZE = 100 * 1024 * 1024 # 100 MiB safety limit
    MAX_METADATA_STRING_SIZE = 1024
    MAX_FRAME_SIZE = MAX_DATA_SIZE + 3*MAX_METADATA_STRING_SIZE + 20

    def __init__(self, data : bytes, job_name : str, machine_name : str, printer_name : str):
        assert len(data) <= self.MAX_DATA_SIZE
        assert len(job_name) <= self.MAX_METADATA_STRING_SIZE
        assert len(machine_name) <= self.MAX_METADATA_STRING_SIZE
        assert len(printer_name) <= self.MAX_METADATA_STRING_SIZE

        self.data = data
        self.job_name = job_name
        self.machine_name = machine_name
        self.printer_name = printer_name

    def encode(self):
        encoded_job_name = self.job_name.encode('utf-8', errors='replace')
        encoded_machine_name = self.machine_name.encode('utf-8', errors='replace')
        encoded_printer_name = self.printer_name.encode('utf-8')
        
        frame = bytes()
        frame += struct.pack('!I', len(self.data))
        frame += struct.pack('!I', len(encoded_job_name))
        frame += struct.pack('!I', len(encoded_machine_name))
        frame += struct.pack('!I', len(encoded_printer_name))
        frame += self.data
        frame += encoded_job_name
        frame += encoded_machine_name
        frame += encoded_printer_name
        frame += struct.pack('!I', zlib.adler32(frame))
        return frame
    
    @classmethod
    def ready_to_decode(cls, frame : bytes):
        if len(frame) < 16:
            return False
        data_len, job_name_len, machine_name_len, printer_name_len = struct.unpack('!IIII', frame[0:16])
        return len(frame) == 20 + data_len + job_name_len + machine_name_len + printer_name_len
    
    @classmethod
    def decode(cls, frame : bytes):
        # get and verify length
        if len(frame) < 16:
            raise ValueError()
        data_len = struct.unpack('!I', frame[0:4])[0]
        job_name_len = struct.unpack('!I', frame[4:8])[0]
        machine_name_len = struct.unpack('!I', frame[8:12])[0]
        printer_name_len = struct.unpack('!I', frame[12:16])[0]
        if len(frame) != 20 + data_len + job_name_len + machine_name_len + printer_name_len:
            raise ValueError()

        # verify the checksum
        if zlib.adler32(frame[:-4]) != struct.unpack('!I', frame[-4:])[0]:
            raise ValueError()
        
        # get data and metadata
        frame_iterator = 16
        data = frame[frame_iterator:frame_iterator+data_len]
        frame_iterator += data_len
        encoded_job_name = frame[frame_iterator:frame_iterator+job_name_len]
        frame_iterator += job_name_len
        encoded_machine_name = frame[frame_iterator:frame_iterator+machine_name_len]
        frame_iterator += machine_name_len
        encoded_printer_name = frame[frame_iterator:frame_iterator+printer_name_len]
        frame_iterator += printer_name_len

        try:
            job_name = encoded_job_name.decode('utf-8')
            machine_name = encoded_machine_name.decode('utf-8')
            printer_name = encoded_printer_name.decode('utf-8')
        except UnicodeDecodeError:
            raise ValueError()
        
        # construct WSPB_frame object
        return cls(data, job_name, machine_name, printer_name)
    
    def __repr__(self) :
        return f'WSPB_frame({repr(self.data)}, {repr(self.job_name)}, {repr(self.machine_name)}, {repr(self.printer_name)})'