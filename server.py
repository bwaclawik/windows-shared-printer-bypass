#!/usr/bin/env python3
import socket, sys, threading, json
import win32service, win32serviceutil, servicemanager
from frame import WSPB_frame
from printer import Printer


class WSPB_server_service(win32serviceutil.ServiceFramework):
    MAX_THREADS = 16
    _svc_name_ = "WSPB-Server-Service"
    _svc_display_name_ = "WSPB Server Service"
    _svc_description_ = "Server service for Windows Shared Printer Bypass"

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.load_config()
        self.active = False
        self.semaphore = threading.Semaphore(self.MAX_THREADS)
    
    def SvcDoRun(self):
        self.active = True
        self.main_loop()

        # wait for thread termination
        for _ in range(self.MAX_THREADS):
            self.semaphore.acquire()

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        self.active = False


    def load_config(self):
        try:
            with open('C:\\WSPB\\server_config.json', 'r') as config_file:
                config = json.load(config_file)

            self.address = (config['address'], config['port'])
            self.printers = config['printers']
        except:
            sys.exit("Cannot correctly parse config file")


    def thread_proc(self, conn : socket.socket, address):
        def log(text):
            print(f'[{address}] : {text}', file=sys.stderr)

        try:
            with conn:
                log('New connection established')
                
                # read frame from socket
                conn.settimeout(30)
                raw_frame = bytes()
                while True:
                    try:
                        new_data = conn.recv(4096)
                    except socket.timeout:
                        break
                    raw_frame += new_data
                    if not new_data or len(raw_frame) > WSPB_frame.MAX_FRAME_SIZE or WSPB_frame.ready_to_decode(raw_frame):
                        break
                log(f'Received total {len(raw_frame)} bytes')

                # decode frame
                try:
                    frame = WSPB_frame.decode(raw_frame)
                    log(f'Decoded frame {repr(frame)}') # only for debug purposes
                except ValueError:
                    log('Frame is ill-formed - aborting')
                    return
                
                # find appropiate printer
                try:
                    local_printer_name = self.printers[frame.printer_name]
                except KeyError:
                    log(f'Unknown printer name {repr(frame.printer_name)} - aborting')
                    return

                # start printing
                try:
                    with Printer(local_printer_name) as printer:
                        with printer.new_print_job(frame.job_name, frame.machine_name) as job:
                            with job.new_page() as page:
                                log('Print job sent')
                                page.write(frame.data)
                except OSError as error:
                    log(f'Windows error "{str(error)}" - aborting')
                    return
                
                # finish
                log('Disconnecting')

        except Exception as exc:
            log(f'Exception "{str(exc)}" has been raised - aborting')

        finally:
            self.semaphore.release()

    
    def main_loop(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as main_socket:
            main_socket.bind(self.address)
            main_socket.settimeout(10)
            main_socket.listen()
            print(f'Listening on {self.address}', file=sys.stderr)

            while self.active:
                try:
                    conn_socket, conn_address = main_socket.accept()
                    self.semaphore.acquire()
                    threading.Thread(target=self.thread_proc, args=(conn_socket, conn_address)).start()
                except socket.timeout:
                    pass



if __name__ == '__main__':
    if len(sys.argv) > 1:
        win32serviceutil.HandleCommandLine(WSPB_server_service)
    else:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(WSPB_server_service)
        servicemanager.StartServiceCtrlDispatcher()